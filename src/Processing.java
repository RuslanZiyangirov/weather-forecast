import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;

public class Processing {



    public ArrayList<Weather> fileProcessing(String address) throws FileNotFoundException, ParseException {


        ArrayList<Weather> listOfWeather = new ArrayList<>();
        FileReader fileReader = new FileReader(address);
        Scanner scanner = new Scanner(fileReader);

        while (scanner.hasNext()){
            Weather weather = new Weather();
            String lineOfFile = scanner.nextLine();
            String[] column = lineOfFile.split(",");

            if (column[0].charAt(0) >= '0' && column[0].charAt(0) <= '9'){

                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd'T'HHmm");

                Date date = simpleDateFormat.parse(column[0]);
                Double temperature = Double.parseDouble(column[1]);
                Double humidity = Double.parseDouble(column[2]);
                Double windSpeed = Double.parseDouble(column[3]);
                Double windDirection = Double.parseDouble(column[4]);

                weather.setDate(date);
                weather.setTemperature(temperature);
                weather.setHumidity(humidity);
                weather.setWindSpeed(windSpeed);
                weather.setWindDirection(windDirection);

                listOfWeather.add(weather);
            }
        }
        return listOfWeather;
    }
}
