
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class DataСollection {


    public String averageAirTemperature(ArrayList<Weather> listOfWeather){
        double averageTemperature = 0.0;

        for (int i = 0; i < listOfWeather.size(); i++) {
            averageTemperature = averageTemperature + listOfWeather.get(i).getTemperature();
        }

        averageTemperature = averageTemperature/listOfWeather.size();

        return Double.toString(averageTemperature);
    }

    public String averageHumidity(ArrayList<Weather> listOfWeather){
        double averageHumidity = 0.0;

        for (int i = 0; i < listOfWeather.size(); i++) {
            averageHumidity = averageHumidity + listOfWeather.get(i).getHumidity();
        }

        averageHumidity = averageHumidity/listOfWeather.size();

        return Double.toString(averageHumidity);
    }

    public String averageWindSpeed(ArrayList<Weather> listOfWeather){
        double averageWindSpeed = 0.0;

        for (int i = 0; i < listOfWeather.size(); i++) {
            averageWindSpeed = averageWindSpeed + listOfWeather.get(i).getWindSpeed();
        }

        averageWindSpeed = averageWindSpeed/listOfWeather.size();

        return  Double.toString(averageWindSpeed);
    }

    public String theDayAndHourWhenTheTemperatureWasHighest(ArrayList<Weather> listOfWeather){
        double max = -1000.0;
        int number = 0;

        for (int i = 0; i < listOfWeather.size(); i++) {
            if(listOfWeather.get(i).getTemperature()>max){
                max = listOfWeather.get(i).getTemperature();
                number = i;
            }
        }

        Date date = listOfWeather.get(number).getDate();
        String dateStr = date.toString().substring(8,10);
        String hourStr = date.toString().substring(11,13);

        return  dateStr+ " число | " + hourStr + "часов.";
    }

    public String theHighestTemperature(ArrayList<Weather> listOfWeather){
        double max = -1000.0;
        for (int i = 0; i < listOfWeather.size(); i++) {
            if(listOfWeather.get(i).getTemperature()>max){
                max = listOfWeather.get(i).getTemperature();
            }
        }
        return Double.toString(max);
    }

    public String theLowestHumidity(ArrayList<Weather> listOfWeather){
        double min = 100.0;

        for (int i = 0; i < listOfWeather.size(); i++) {
            if(listOfWeather.get(i).getHumidity()<min){
                min = listOfWeather.get(i).getHumidity();
            }
        }
        return Double.toString(min);
    }

    public String theStrongestWind(ArrayList<Weather> listOfWeather){
        double max = -1.0;

        for (int i = 0; i < listOfWeather.size(); i++) {
            if(listOfWeather.get(i).getWindSpeed()>max){
                max = listOfWeather.get(i).getWindSpeed();
            }
        }
        return Double.toString(max);
    }

    public String mostFrequentWindDirection(ArrayList<Weather> listOfWeather){

        //у каждого объекта определить направление ветра, после этого под каждое из направлений сделать счетчик и выбрать наибольшее
        int north = 0;
        int northeastern = 0;
        int eastern = 0;
        int southeastern = 0;
        int south = 0;
        int southwestern = 0;
        int west = 0;
        int northwestern = 0;

        for (int i = 0; i < listOfWeather.size(); i++) {

            if (listOfWeather.get(i).getWindDirection()>=0 && listOfWeather.get(i).getWindDirection()<22.5){
                north++;
            }
            if (listOfWeather.get(i).getWindDirection()>=22.5 && listOfWeather.get(i).getWindDirection()<45){
                northeastern++;
            }
            if (listOfWeather.get(i).getWindDirection()>=45 && listOfWeather.get(i).getWindDirection()<67.5){
                northeastern++;
            }
            if (listOfWeather.get(i).getWindDirection()>= 67.5 && listOfWeather.get(i).getWindDirection()<90){
                eastern++;
            }
            if (listOfWeather.get(i).getWindDirection()>= 90 && listOfWeather.get(i).getWindDirection()<112.5){
                eastern++;
            }
            if (listOfWeather.get(i).getWindDirection()>= 112.5 && listOfWeather.get(i).getWindDirection()<135){
                southeastern++;
            }
            if (listOfWeather.get(i).getWindDirection()>= 135 && listOfWeather.get(i).getWindDirection()<157.5){
                southeastern++;
            }
            if (listOfWeather.get(i).getWindDirection()>= 157.5 && listOfWeather.get(i).getWindDirection()<180){
                south++;
            }
            if (listOfWeather.get(i).getWindDirection()>= 180 && listOfWeather.get(i).getWindDirection()<202.5){
                south++;
            }
            if (listOfWeather.get(i).getWindDirection()>= 202.5 && listOfWeather.get(i).getWindDirection()<225){
                southwestern++;
            }
            if (listOfWeather.get(i).getWindDirection()>= 225 && listOfWeather.get(i).getWindDirection()<247.5){
                southwestern++;
            }
            if (listOfWeather.get(i).getWindDirection()>= 247.5 && listOfWeather.get(i).getWindDirection()<270){
                west++;
            }
            if (listOfWeather.get(i).getWindDirection()>= 270 && listOfWeather.get(i).getWindDirection()<292.5){
                west++;
            }
            if (listOfWeather.get(i).getWindDirection()>= 292.5 && listOfWeather.get(i).getWindDirection()<315){
                northwestern++;
            }
            if (listOfWeather.get(i).getWindDirection()>= 315 && listOfWeather.get(i).getWindDirection()<337.5){
                northwestern++;
            }
            if (listOfWeather.get(i).getWindDirection()>= 337.5 && listOfWeather.get(i).getWindDirection()<=360){
                north++;
            }
        }

        if(north > northeastern && north > eastern && north > southeastern && north > south && north > southwestern &&
        north > west && north > northwestern){
            return "Северное";
        }
        if (northeastern > north && northeastern > eastern && northeastern > southeastern && northeastern > south &&
                northeastern > southwestern && northeastern > west && northeastern > northwestern){
            return "Северо - восточное";
        }
        if (eastern > north && eastern > northeastern && eastern > southeastern && eastern > south &&
                eastern > southwestern && eastern > west && eastern > northwestern){
            return "Восточный";
        }
        if (southeastern > north && southeastern > northeastern && southeastern > eastern && southeastern > south &&
        southeastern > southwestern && southeastern > west && southeastern > northwestern ){
            return "Юго - восточный";
        }
        if (south > north && south > northeastern && south > eastern && south > southeastern && south > southwestern &&
        south > west && south > northwestern){
            return "Южный";
        }
        if (southwestern > north && southwestern > northeastern && southwestern > eastern && southwestern > southeastern
            && southwestern > south && southwestern > west && southwestern > northwestern){
            return "Юго - западный";
        }
        if (west > north && west > northeastern && west > northwestern && west > eastern && west > southeastern &&
                west > south && west > southwestern){
            return "Западный";
        }
        if (northwestern > north && northwestern > northeastern && northwestern > eastern && northwestern > southeastern
            && northwestern > south && northwestern > southwestern && northwestern > west){
            return "Северо - Западный";
        }

        return "";
    }


}
