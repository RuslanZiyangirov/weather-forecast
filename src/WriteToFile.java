import java.io.*;

import java.util.ArrayList;

public class WriteToFile {


    public void writeToFile(String address,String fileWriteAddress) throws Exception {
        Processing processing = new Processing();

        DataСollection dataСollection = new DataСollection();
        ArrayList<Weather> listOfWeather = processing.fileProcessing(address);

        FileWriter fileWriter = new FileWriter(fileWriteAddress);
        BufferedWriter bufferedWrite = new BufferedWriter(fileWriter);
        bufferedWrite.write("Средняя температура воздуха: " + dataСollection.averageAirTemperature(listOfWeather) +"\n");
        bufferedWrite.write("Cредняя влажность воздуха: " + dataСollection.averageHumidity(listOfWeather) +"\n");
        bufferedWrite.write("Cредняя скорость ветра: " + dataСollection.averageWindSpeed(listOfWeather) +"\n");
        bufferedWrite.write("День и час: "+ dataСollection.theDayAndHourWhenTheTemperatureWasHighest(listOfWeather) + ". Самая высокая темперутра: " +
                dataСollection.theHighestTemperature(listOfWeather) + " градусов" +"\n");
        bufferedWrite.write("Cамая низкая влажность: " + dataСollection.theLowestHumidity(listOfWeather) +"%" +"\n");
        bufferedWrite.write("Самый сильный ветер: " + dataСollection.theStrongestWind(listOfWeather) +"\n");
        bufferedWrite.write("Cамое частое направление ветра: " + dataСollection.mostFrequentWindDirection(listOfWeather) +"\n");
        bufferedWrite.close();

    }
}
