import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.ParseException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws Exception {
        System.out.println("Укажите путь к файлу с погодой.");
        Scanner scanner = new Scanner(System.in);
        String address = scanner.nextLine();
        System.out.println("Укажите путь для создания нового файла.");
        String fileWriteAddress = scanner.nextLine();
        WriteToFile writeToFile = new WriteToFile();
        writeToFile.writeToFile(address,fileWriteAddress);

    }
}
